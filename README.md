ANZ FX Calculator
==================

Background
----------
This project is for Programming Exercise – FX Calculator.
The task is to create a console-based currency converter application.

The application allows a user to convert an amount in a specific currency to the equivalent amount in another currency.
Your calculator should allow a user to enter an amount in any of the known currencies, and provide the equivalent amount in another currency.
Your calculator should parse console input like "<ccy1> <amount1> in <ccy2>", and provide a suitable response.
For example:

```
%> AUD 100.00 in USD
AUD 100.00 = USD 83.71
%> AUD 100.00 in AUD
AUD 100.00 = AUD 100.00
%> AUD 100.00 in DKK
AUD 100.00 = DKK 505.76
%> JPY 100 in USD
JPY 100 = USD 0.83
```

If the rate cannot be calculated, the program should alert the user:

```
%> KRW 1000.00 in FJD
```

Read document file [MWLD-programming-test-fxcalculator-1-0.pdf](docs/MWLD-programming-test-fxcalculator-1-0.pdf) for more information.

Code Structure
---------------

There are two sub-projects in the root project anz-fx-calculator.

* **fxcalculator-core**   The core library of the calculator, including major of the logical and calculation.
* **fxcalculator-cli**   The user 'command line interface' application of calculator.


Getting Started
----------------

The main entry class is **me.ucu.exam.anz.fxcalculator.cli.Application** in **fxcalculator-cli** sub-prject.

The program privde continuous interaction via 'command line user interface'.

```
usage:
	AUD 10000 in USD

commands:
	q    - quit the calculator
	quit - same as q
	exit - same as q
	help - show this information
```

License
--------

This is released under the [BSD-3 license](LICENSE.md).

