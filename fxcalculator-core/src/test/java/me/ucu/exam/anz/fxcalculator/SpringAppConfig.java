package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.MemEmbeddedCurrencyReader;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Rui Zhou on 2017/4/25.
 */
@Configuration
public class SpringAppConfig {

    @Bean
    @ConfigurationProperties("fxcalculator")
    public MemEmbeddedCurrencyReader currencyReader() {
        return new MemEmbeddedCurrencyReader();
    }

}
