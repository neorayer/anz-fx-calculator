package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.currency.CurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.NoSuchCurrencyException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rui Zhou on 2017/4/25.
 */
public abstract class CurrencyReaderTest {

    protected CurrencyReader currencyReader;

    @Before
    public abstract void initCurrencyReader();

    @Test
    public void read() throws NoSuchCurrencyException {
        // Given
        Currency aud = currencyReader.read("AUD");
        Currency cad = currencyReader.read("CAD");
        Currency cny = currencyReader.read("CNY");
        Currency dkk = currencyReader.read("DKK");
        Currency eur = currencyReader.read("EUR");
        Currency gbp = currencyReader.read("GBP");
        Currency jpy = currencyReader.read("JPY");
        Currency nok = currencyReader.read("NOK");
        Currency nzd = currencyReader.read("NZD");
        Currency usd = currencyReader.read("USD");

        // Then
        assertEquals("AUD", aud.getName());
        assertEquals(2, aud.getDispDecimalPlaces());
        assertEquals("CAD", cad.getName());
        assertEquals(2, cad.getDispDecimalPlaces());
        assertEquals("CNY", cny.getName());
        assertEquals(2, cny.getDispDecimalPlaces());
        assertEquals("DKK", dkk.getName());
        assertEquals(2, dkk.getDispDecimalPlaces());
        assertEquals("EUR", eur.getName());
        assertEquals(2, eur.getDispDecimalPlaces());
        assertEquals("GBP", gbp.getName());
        assertEquals(2, gbp.getDispDecimalPlaces());
        assertEquals("JPY", jpy.getName());
        assertEquals(0, jpy.getDispDecimalPlaces());
        assertEquals("NOK", nok.getName());
        assertEquals(2, nok.getDispDecimalPlaces());
        assertEquals("NZD", nzd.getName());
        assertEquals(2, nzd.getDispDecimalPlaces());
        assertEquals("USD", usd.getName());
        assertEquals(2, usd.getDispDecimalPlaces());
    }

    @Test
    public void readWithLowercase() throws NoSuchCurrencyException {
        // Given
        Currency usd = currencyReader.read("usd");

        // Then
        assertEquals("USD", usd.getName());
        assertEquals(2, usd.getDispDecimalPlaces());
    }

    @Test(expected = NoSuchCurrencyException.class)
    public void readFailed() throws NoSuchCurrencyException {
        try {
            currencyReader.read("XXX");
        } catch (NoSuchCurrencyException e) {
            assertEquals("XXX", e.getCurrencyName());
            throw e;
        }
    }


}
