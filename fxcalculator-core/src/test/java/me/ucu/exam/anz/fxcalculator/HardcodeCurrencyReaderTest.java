package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.HardcodeCurrencyReader;

/**
 * Created by Rui Zhou on 2017/4/24.
 */
public class HardcodeCurrencyReaderTest extends CurrencyReaderTest {

    @Override
    public void initCurrencyReader() {
        currencyReader = new HardcodeCurrencyReader();
    }
}
