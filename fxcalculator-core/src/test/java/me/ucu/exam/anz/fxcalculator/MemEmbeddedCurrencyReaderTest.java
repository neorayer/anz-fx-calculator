package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.MemEmbeddedCurrencyReader;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

/**
 * Created by Rui Zhou on 2017/4/24.
 */
@SpringBootTest(webEnvironment = WebEnvironment.NONE, classes = SpringAppConfig.class)
@RunWith(SpringRunner.class)
@EnableAutoConfiguration
public class MemEmbeddedCurrencyReaderTest extends CurrencyReaderTest {

    @Autowired
    private MemEmbeddedCurrencyReader memEmbeddedCurrencyReader;

    @Override
    public void initCurrencyReader() {
        currencyReader = memEmbeddedCurrencyReader;
    }
}
