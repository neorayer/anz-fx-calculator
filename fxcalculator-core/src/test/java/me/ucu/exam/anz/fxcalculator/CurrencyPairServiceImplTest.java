package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.pair.CurrencyPair;
import me.ucu.exam.anz.fxcalculator.pair.DirectCurrencyPair;
import me.ucu.exam.anz.fxcalculator.service.CurrencyPairServiceImpl;
import me.ucu.exam.anz.fxcalculator.service.UnableToFindRateException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rui Zhou on 2017/4/24.
 */
public class CurrencyPairServiceImplTest {

    private final Currency AUD = new Currency("AUD", 2);
    private final Currency CAD = new Currency("CAD", 2);
    private final Currency CZK = new Currency("CZK", 2);
    private final Currency DKK = new Currency("DKK", 2);
    private final Currency EUR = new Currency("EUR", 2);
    private final Currency GBP = new Currency("GBP", 2);
    private final Currency JPY = new Currency("JPY", 0);
    private final Currency NOK = new Currency("NOK", 2);
    private final Currency NZD = new Currency("NZD", 2);
    private final Currency USD = new Currency("USD", 2);

    private CurrencyPairServiceImpl rateReader = new CurrencyPairServiceImpl();

    @Before
    public void setUp() {
    }

    @Test
    public void readDirect() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        CurrencyPair pair = rateReader.getPair(AUD, USD);

        // Then
        assertEquals(0.8371d, pair.getRate(), 0.0001d);
    }

    @Test
    public void readInvert() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        CurrencyPair pair = rateReader.getPair(USD, AUD);

        // Then
        assertEquals(1.1946d, pair.getRate(), 0.0001d);
    }

    @Test
    public void readUnity() throws UnableToFindRateException {
        // Given

        // When
        CurrencyPair pair = rateReader.getPair(AUD, AUD);

        // Then
        assertEquals(1d, pair.getRate(), 0.0001d);
    }

    /**
     * test cross via USD
     *
     * @throws UnableToFindRateException
     */
    @Test
    public void readCross1() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));
        rateReader.put(new DirectCurrencyPair(CAD, USD, 4, 0.8711d));

        // When
        CurrencyPair pair = rateReader.getPair(AUD, CAD);

        // Then
        assertEquals(0.9610d, pair.getRate(), 0.0001d);
    }

    @Test
    public void readCross2() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(EUR, CZK, 4, 27.6028d));
        rateReader.put(new DirectCurrencyPair(EUR, NOK, 4, 8.6651d));

        // When
        CurrencyPair pair = rateReader.getPair(NOK, CZK);

        // Then
        assertEquals(3.1855d, pair.getRate(), 0.0001d);
    }

    @Test(expected = UnableToFindRateException.class)
    public void readFailed() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));
        rateReader.put(new DirectCurrencyPair(CAD, USD, 4, 0.8711d));

        // When
        rateReader.getPair(AUD, NZD);

        // Given
        // the UnableToFindRateException is expected
    }

}
