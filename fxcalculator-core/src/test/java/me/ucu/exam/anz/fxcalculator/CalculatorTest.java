package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.currency.CurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.HardcodeCurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.NoSuchCurrencyException;
import me.ucu.exam.anz.fxcalculator.pair.DirectCurrencyPair;
import me.ucu.exam.anz.fxcalculator.service.CurrencyPairServiceImpl;
import me.ucu.exam.anz.fxcalculator.service.UnableToFindRateException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rui Zhou on 2017/4/24.
 */
public class CalculatorTest {

    private final Currency AUD = new Currency("AUD", 2);
    private final Currency CAD = new Currency("CAD", 2);
    private final Currency JPY = new Currency("JPY", 0);
    private final Currency USD = new Currency("USD", 2);

    private CurrencyPairServiceImpl rateReader = new CurrencyPairServiceImpl();

    private Calculator calculator = new Calculator(rateReader);

    @Test
    public void convertDirect() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        double termsAmount = calculator.convert(AUD, USD, 12345678d);

        // Then
        assertEquals(10334567.0538d, termsAmount, 0.0001d);
    }

    @Test
    public void convertInvert() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        double termsAmount = calculator.convert(USD, AUD, 12345678d);

        // Then
        assertEquals(14748151.9531d, termsAmount, 0.0001d);
    }


    @Test
    public void convertUnity() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        double termsAmount = calculator.convert(USD, USD, 12345678d);

        // Then
        assertEquals(12345678d, termsAmount, 0.0001d);
    }

    @Test
    public void convertCross() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));
        rateReader.put(new DirectCurrencyPair(CAD, USD, 4, 0.8711d));

        // When
        double termsAmount = calculator.convert(AUD, CAD, 12345678d);

        // Then
        assertEquals(11863812.4828d, termsAmount, 0.0001d);
    }

    @Test(expected = UnableToFindRateException.class)
    public void convertFailed() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        double termsAmount = calculator.convert(AUD, CAD, 12345678d);

        // Then
        // expect to throw a UnableToFindRateException
    }

    @Test
    public void convertToString_USD() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));

        // When
        String termsAmount = calculator.convertToString(USD, AUD, 12345678d);

        // Then
        assertEquals("14,748,151.95", termsAmount);
    }

    @Test
    public void convertToString_JPY() throws UnableToFindRateException {
        // Given
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));
        rateReader.put(new DirectCurrencyPair(USD, JPY, 2, 119.95d));

        // When
        String termsAmount = calculator.convertToString(AUD, JPY, 12345678d);

        // Then
        assertEquals("1,239,631,318", termsAmount);
    }

    @Test
    public void convertToDescription() throws UnableToFindRateException, NoSuchCurrencyException {
        // Given
        CurrencyReader currencyReader = new HardcodeCurrencyReader();
        Calculator calculator = new Calculator(currencyReader, rateReader);
        rateReader.put(new DirectCurrencyPair(AUD, USD, 4, 0.8371d));
        rateReader.put(new DirectCurrencyPair(USD, JPY, 2, 119.95d));

        // When
        String termsAmount = calculator.convertToDescription("aud", "jpy", 12345678d);

        // Then
        assertEquals("AUD 12,345,678.00 = JPY 1,239,631,318", termsAmount);
    }
}

