package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.pair.CurrencyPair;
import me.ucu.exam.anz.fxcalculator.pair.DirectCurrencyPair;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Rui Zhou on 2017/4/24.
 */
public class CurrencyPairTest {

    private final Currency AUD = new Currency("AUD", 2);
    private final Currency CAD = new Currency("CAD", 2);

    @Test
    public void name() {
        // Given
        CurrencyPair pair = new DirectCurrencyPair(AUD, CAD, 4, 0.8371);

        // When
        String name = pair.getName();

        // Then
        assertEquals("AUD/CAD", name);
    }

    @Test
    public void testToString() {
        // Given
        CurrencyPair pair = new DirectCurrencyPair(AUD, CAD, 4, 0.8371);

        // When
        String str = pair.toString();

        // Then
        assertEquals("AUD/CAD", str);
    }


}
