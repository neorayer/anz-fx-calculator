package me.ucu.exam.anz.fxcalculator;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.currency.CurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.NoSuchCurrencyException;
import me.ucu.exam.anz.fxcalculator.pair.CurrencyPair;
import me.ucu.exam.anz.fxcalculator.service.CurrencyPairService;
import me.ucu.exam.anz.fxcalculator.service.UnableToFindRateException;

/**
 * The core class of fx calculator core library.
 */
public class Calculator {

    private CurrencyReader currencyReader;

    private CurrencyPairService currencyPairService;

    public Calculator(CurrencyReader currencyReader, CurrencyPairService currencyPairService) {
        this.currencyReader = currencyReader;
        this.currencyPairService = currencyPairService;
    }

    public Calculator(CurrencyPairService currencyPairService) {
        this(null, currencyPairService);
    }

    /**
     * get terms currency amount by the given base currency, terms currency and the base amount.
     *
     * @param baseCurrency  the base currency
     * @param termsCurrency the terms currency
     * @param baseAmount    the base amount need to be converted
     * @return the amount of terms currency
     * @throws UnableToFindRateException if the currency pair cannot be found in system
     */
    public double convert(Currency baseCurrency, Currency termsCurrency, double baseAmount) throws UnableToFindRateException {
        CurrencyPair currencyPair = currencyPairService.getPair(baseCurrency, termsCurrency);
        return currencyPair.getRate() * baseAmount;
    }

    /**
     * get terms currency amount by the given base currency, terms currency and the base amount.
     * The return result is convert to a formatted string.
     *
     * @param baseCurrency  the base currency
     * @param termsCurrency the terms currency
     * @param baseAmount    the base amount need to be converted
     * @return the formatted string of the amount of terms currency
     * @throws UnableToFindRateException if the currency pair cannot be found in system
     */
    public String convertToString(Currency baseCurrency, Currency termsCurrency, double baseAmount) throws UnableToFindRateException {
        CurrencyPair currencyPair = currencyPairService.getPair(baseCurrency, termsCurrency);
        double termsAmount = currencyPair.getRate() * baseAmount;
        return termsCurrency.displayAmount(termsAmount);
    }

    /**
     * get terms currency amount by the given base currency name, terms currency name and the base amount.
     * The return result is convert to a formatted description string.
     *
     * @param baseCurrencyName  the string name of base currency
     * @param termsCurrencyName the string name of terms currency
     * @param baseAmount        the amount of base currency need to be converted
     * @return the formatted description string of the converting result
     * @throws NoSuchCurrencyException   if one of base or terms currencies cannot be found in system
     * @throws UnableToFindRateException if the currency pair cannot be found in system
     */
    public String convertToDescription(String baseCurrencyName, String termsCurrencyName, double baseAmount)
            throws NoSuchCurrencyException, UnableToFindRateException {
        Currency baseCurrency = currencyReader.read(baseCurrencyName);
        Currency termsCurrency = currencyReader.read(termsCurrencyName);
        double termsAmount = convert(baseCurrency, termsCurrency, baseAmount);
        return String.format("%s %s = %s %s",
                baseCurrency.getName(),
                baseCurrency.displayAmount(baseAmount),
                termsCurrency.getName(),
                termsCurrency.displayAmount(termsAmount));
    }
}
