package me.ucu.exam.anz.fxcalculator.currency;

import java.text.DecimalFormat;

/**
 * The currency model class.
 */
public class Currency {

    private String name;

    private int dispDecimalPlaces;

    public Currency(String name, int dispDecimalPlaces) {
        setName(name);
        setDispDecimalPlaces(dispDecimalPlaces);
    }

    public Currency() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name.toUpperCase();
    }

    public int getDispDecimalPlaces() {
        return dispDecimalPlaces;
    }

    public void setDispDecimalPlaces(int dispDecimalPlaces) {
        this.dispDecimalPlaces = dispDecimalPlaces;
    }

    /**
     * convert the double type amount into string with format of right dispDecimalPlaces.
     *
     * @param amount the double type value need to be transfer
     * @return the formatted string of value amount
     */
    public String displayAmount(double amount) {
        String pattern = "#,###";
        if (dispDecimalPlaces > 0) {
            pattern += ".";
            for (int i = 0; i < dispDecimalPlaces; i++) {
                pattern += '0';
            }
        }
        DecimalFormat df = new DecimalFormat(pattern);
        return df.format(amount);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public boolean equals(Object object) {
        if (object instanceof Currency) {
            return name.equalsIgnoreCase(((Currency) object).getName());
        }
        return false;
    }

}
