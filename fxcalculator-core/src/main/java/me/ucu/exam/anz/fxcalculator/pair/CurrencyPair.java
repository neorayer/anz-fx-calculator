package me.ucu.exam.anz.fxcalculator.pair;

import me.ucu.exam.anz.fxcalculator.currency.Currency;

/**
 * The abstract currency pair class. Normally, it has four derived classes:
 * {@see DirectCurrencyPair }
 * {@see InvertCurrencyPair }
 * {@see UnityCurrencyPair }
 * {@see CrossCurrency }
 */
public abstract class CurrencyPair {

    private final String name;
    private Currency baseCurrency;
    private Currency termsCurrency;
    private int decimalPlaces;

    public CurrencyPair(Currency baseCurrency, Currency termsCurrency, int decimalPlaces) {
        this.baseCurrency = baseCurrency;
        this.termsCurrency = termsCurrency;
        this.decimalPlaces = decimalPlaces;

        this.name = baseCurrency.getName() + "/" + termsCurrency.getName();
    }

    public Currency getBaseCurrency() {
        return baseCurrency;
    }

    public Currency getTermsCurrency() {
        return termsCurrency;
    }

    public int getDecimalPlaces() {
        return decimalPlaces;
    }

    public String getName() {
        return name;
    }

    public abstract double getRate();


    @Override
    public String toString() {
        return String.format("%s", name);
    }
}
