package me.ucu.exam.anz.fxcalculator.currency;

import java.util.ArrayList;
import java.util.List;

/**
 * Another simple implementation of interface CurrencyReader. It keeps the currencies
 * information in a ArrayList in memory.  MemEmbeddedCurrencyReader can be set currency
 * information dynamically via its setCurrencies() method.
 */
public class MemEmbeddedCurrencyReader implements CurrencyReader {

    private List<Currency> currencies = new ArrayList<>();

    /**
     * Get a Currency object via string name
     *
     * @param currencyName the currency name
     * @return the corresponding Currency object
     * @throws NoSuchCurrencyException if cannot read the corresponding Currency object correctly
     */
    @Override
    public Currency read(String currencyName) throws NoSuchCurrencyException {
        return currencies.stream()
                .filter(currency -> currencyName.trim().equalsIgnoreCase(currency.getName()))
                .findFirst()
                .orElseThrow(() -> new NoSuchCurrencyException(currencyName));
    }

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public void setCurrencies(List<Currency> currencies) {
        this.currencies = currencies;
    }
}
