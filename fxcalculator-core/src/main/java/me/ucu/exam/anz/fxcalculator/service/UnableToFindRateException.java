package me.ucu.exam.anz.fxcalculator.service;

import me.ucu.exam.anz.fxcalculator.currency.Currency;

/**
 * UnableToFindRateException is thrown if the currency pair cannot be found int the system.
 */
public class UnableToFindRateException extends Exception {

    private Currency baseCurrency;

    private Currency termsCurrency;

    public UnableToFindRateException(Currency baseCurrency, Currency termsCurrency) {
        super("Cannot find pair pair " + baseCurrency.getName() + "/" + termsCurrency.getName());
        this.baseCurrency = baseCurrency;
        this.termsCurrency = termsCurrency;
    }

    public String getCurrencyPairName() {
        return this.baseCurrency.getName() + "/" + this.termsCurrency.getName();
    }
}
