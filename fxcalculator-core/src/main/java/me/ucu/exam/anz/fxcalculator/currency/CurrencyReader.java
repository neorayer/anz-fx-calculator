package me.ucu.exam.anz.fxcalculator.currency;

/**
 * The interface defined to read a Currency object from a string name.
 */
public interface CurrencyReader {

    /**
     * Get a Currency object via string name
     *
     * @param currencyName the currency name
     * @return the corresponding Currency object
     * @throws NoSuchCurrencyException if cannot read the corresponding Currency object correctly
     */
    Currency read(String currencyName) throws NoSuchCurrencyException;
}
