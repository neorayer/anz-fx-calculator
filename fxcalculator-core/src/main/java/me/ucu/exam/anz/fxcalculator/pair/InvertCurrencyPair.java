package me.ucu.exam.anz.fxcalculator.pair;

/**
 * Derived class of CurrrencyPair.
 * It's the inverted of DirectCurrencyPair.
 * The currency pair can be looked up if base and terms are flipped (and rate = 1/rate)
 */
public class InvertCurrencyPair extends CurrencyPair {

    private DirectCurrencyPair directCurrencyPair;

    public InvertCurrencyPair(DirectCurrencyPair directCurrencyPair) {
        super(directCurrencyPair.getTermsCurrency(),
                directCurrencyPair.getBaseCurrency(),
                directCurrencyPair.getDecimalPlaces());
        this.directCurrencyPair = directCurrencyPair;
    }

    @Override
    public double getRate() {
        return 1 / directCurrencyPair.getRate();
    }

}
