package me.ucu.exam.anz.fxcalculator.service;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.pair.CurrencyPair;

/**
 * The interface to define the services of CurrencyPairService.
 */
public interface CurrencyPairService {

    /**
     * Get a CurrencyPair object from the base currency and terms currency parameters.
     *
     * @param baseCurrency  the base currency object
     * @param termsCurrency the terms currency object
     * @return the corresponding CurrencyPair object
     * @throws UnableToFindRateException if the currency pair can't be found in system
     */
    CurrencyPair getPair(Currency baseCurrency, Currency termsCurrency) throws UnableToFindRateException;
}
