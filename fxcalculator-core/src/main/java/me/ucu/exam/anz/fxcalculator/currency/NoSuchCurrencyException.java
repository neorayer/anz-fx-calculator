package me.ucu.exam.anz.fxcalculator.currency;

/**
 * NoSuchCurrencyException is thrown if the currency cannot be found in system.
 */
public class NoSuchCurrencyException extends Exception {

    private String currencyName;

    public NoSuchCurrencyException(String currencyName) {
        super("Unable to find currency by name " + currencyName);
        this.currencyName = currencyName;
    }

    public String getCurrencyName() {
        return currencyName;
    }
}
