package me.ucu.exam.anz.fxcalculator.currency;

/**
 * A hardcode implementation of interface CurrencyReader. The major purpose of this class
 * is for simplifying the unit test and logic validation.
 */
public class HardcodeCurrencyReader implements CurrencyReader {

    /**
     * Get a Currency object via string name
     *
     * @param currencyName the currency name
     * @return the corresponding Currency object
     * @throws NoSuchCurrencyException if cannot read the corresponding Currency object correctly
     */
    @Override
    public Currency read(String currencyName) throws NoSuchCurrencyException {
        String name = currencyName.trim().toUpperCase();
        switch (name) {
            case "AUD":
                return new Currency(name, 2);
            case "CAD":
                return new Currency(name, 2);
            case "CNY":
                return new Currency(name, 2);
            case "CZK":
                return new Currency(name, 2);
            case "DKK":
                return new Currency(name, 2);
            case "EUR":
                return new Currency(name, 2);
            case "GBP":
                return new Currency(name, 2);
            case "JPY":
                return new Currency(name, 0);
            case "NOK":
                return new Currency(name, 2);
            case "NZD":
                return new Currency(name, 2);
            case "USD":
                return new Currency(name, 2);
            default:
                throw new NoSuchCurrencyException(currencyName);
        }
    }
}
