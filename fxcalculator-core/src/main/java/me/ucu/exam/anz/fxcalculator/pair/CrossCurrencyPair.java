package me.ucu.exam.anz.fxcalculator.pair;

/**
 * Derived class of CurrrencyPair.
 * In order to calculate this rate, you need to cross via a 'agent' currency.
 */
public class CrossCurrencyPair extends CurrencyPair {

    private CurrencyPair baseToCrossPair;

    private CurrencyPair crossToTermsPair;

    public CrossCurrencyPair(CurrencyPair baseToCrossPair, CurrencyPair crossTotermsPair) {
        super(baseToCrossPair.getBaseCurrency(), crossTotermsPair.getTermsCurrency(), crossTotermsPair.getDecimalPlaces());
        this.baseToCrossPair = baseToCrossPair;
        this.crossToTermsPair = crossTotermsPair;
    }

    @Override
    public double getRate() {
        return baseToCrossPair.getRate() * crossToTermsPair.getRate();
    }

}
