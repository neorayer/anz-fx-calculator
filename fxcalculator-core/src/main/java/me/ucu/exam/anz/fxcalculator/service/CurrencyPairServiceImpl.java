package me.ucu.exam.anz.fxcalculator.service;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.pair.*;

import java.util.concurrent.ConcurrentHashMap;

/**
 * An implementation of interface CurrencyPairService.
 * CurrencyPairServiceImpl class uses two compact multiplex ConcurrentHashMap to
 * store the direct currency pairs.
 * The other types of currency pairs are created via the existing direct currency pairs.
 * <p>
 * For the using of ConcurrentHashMap, this class is thread safe and supporting concurrent
 * invocation.
 */
public class CurrencyPairServiceImpl implements CurrencyPairService {

    private ConcurrentHashMap<Currency, ConcurrentHashMap<Currency, DirectCurrencyPair>> directMatrix = new ConcurrentHashMap<>();

    private ConcurrentHashMap<Currency, ConcurrentHashMap<Currency, CurrencyPair>> richMatrix = new ConcurrentHashMap<>();

    private static <T extends CurrencyPair> void addPair(ConcurrentHashMap<Currency, ConcurrentHashMap<Currency, T>> matrix, T pair) {
        ConcurrentHashMap<Currency, T> rowMap = matrix.get(pair.getBaseCurrency());
        if (rowMap == null) {
            rowMap = new ConcurrentHashMap<>();
            matrix.put(pair.getBaseCurrency(), rowMap);
        }
        rowMap.put(pair.getTermsCurrency(), pair);
    }

    private static <T extends CurrencyPair> T get(ConcurrentHashMap<Currency, ConcurrentHashMap<Currency, T>> matrix,
                                                  Currency baseCurrency,
                                                  Currency termsCurrency) {
        ConcurrentHashMap<Currency, T> rowMap = matrix.get(baseCurrency);
        return rowMap == null ? null : rowMap.get(termsCurrency);
    }

    public void put(DirectCurrencyPair directCurrencyPair) {
        addPair(directMatrix, directCurrencyPair);
        addPair(richMatrix, directCurrencyPair);
        addPair(richMatrix, new InvertCurrencyPair(directCurrencyPair));
    }


    /**
     * Get a CurrencyPair object from the base currency and terms currency parameters.
     *
     * @param baseCurrency  the base currency object
     * @param termsCurrency the terms currency object
     * @return the corresponding CurrencyPair object
     * @throws UnableToFindRateException if the currency pair can't be found in system
     */
    @Override
    public CurrencyPair getPair(Currency baseCurrency, Currency termsCurrency) throws UnableToFindRateException {
        // unity
        if (baseCurrency.equals(termsCurrency)) {
            return new UnityCurrencyPair(baseCurrency);
        }

        // direct
        DirectCurrencyPair directCurrencyPair = readDirect(baseCurrency, termsCurrency);
        if (directCurrencyPair != null) {
            return directCurrencyPair;
        }

        // inverted
        InvertCurrencyPair invertCurrencyPair = readInvert(baseCurrency, termsCurrency);
        if (invertCurrencyPair != null) {
            return invertCurrencyPair;
        }

        // cross
        CrossCurrencyPair crossCurrencyPair = readCross(baseCurrency, termsCurrency);
        if (crossCurrencyPair != null) {
            return crossCurrencyPair;
        }

        throw new UnableToFindRateException(baseCurrency, termsCurrency);
    }

    private DirectCurrencyPair readDirect(Currency baseCurrency, Currency termsCurrency) {
        return get(directMatrix, baseCurrency, termsCurrency);
    }

    private InvertCurrencyPair readInvert(Currency baseCurrency, Currency termsCurrency) {
        DirectCurrencyPair dirPair = readDirect(termsCurrency, baseCurrency);
        return dirPair == null ? null : new InvertCurrencyPair(dirPair);
    }

    private CrossCurrencyPair readCross(Currency baseCurrency, Currency termsCurrency) {
        ConcurrentHashMap<Currency, CurrencyPair> baseToCrossPairs = richMatrix.get(baseCurrency);
        if (baseToCrossPairs == null) {
            return null;
        }

        for (CurrencyPair baseToCrossPair : baseToCrossPairs.values()) {
            CurrencyPair crossToTermsPair = crossToTermsPair = get(richMatrix, baseToCrossPair.getTermsCurrency(), termsCurrency);
            if (crossToTermsPair != null) {
                return new CrossCurrencyPair(baseToCrossPair, crossToTermsPair);
            }
        }

        return null;
    }
}
