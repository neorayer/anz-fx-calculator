package me.ucu.exam.anz.fxcalculator.pair;

import me.ucu.exam.anz.fxcalculator.currency.Currency;

/**
 * Derived class of CurrrencyPair.
 * The baseCurrency is identical with the termsCurrency, and the rate is always 1.
 */
public class UnityCurrencyPair extends CurrencyPair {

    public UnityCurrencyPair(Currency baseCurrency) {
        super(baseCurrency, baseCurrency, 0);
    }

    @Override
    public double getRate() {
        return 1d;
    }
}
