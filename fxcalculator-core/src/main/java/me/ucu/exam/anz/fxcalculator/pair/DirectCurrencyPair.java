package me.ucu.exam.anz.fxcalculator.pair;


import me.ucu.exam.anz.fxcalculator.currency.Currency;

/**
 * Derived class of CurrrencyPair.
 * It's direct feed - the currency pair can be looked up directly.
 */
public class DirectCurrencyPair extends CurrencyPair {

    private final double rate;

    public DirectCurrencyPair(Currency baseCurrency, Currency termsCurrency, int decimalPlaces, double rate) {
        super(baseCurrency, termsCurrency, decimalPlaces);
        this.rate = rate;
    }

    @Override
    public double getRate() {
        return rate;
    }
}
