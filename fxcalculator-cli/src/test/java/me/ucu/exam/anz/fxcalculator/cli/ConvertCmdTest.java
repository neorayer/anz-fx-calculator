package me.ucu.exam.anz.fxcalculator.cli;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConvertCmdTest {

    @Test
    public void parse() throws ConvertCmd.NotConvertCommandException {
        // Given
        String cmdLine = "AUD 100.00 in USD";

        // When
        ConvertCmd convertCmd = ConvertCmd.parse(cmdLine);

        // Then
        assertEquals("AUD", convertCmd.getBaseCurrencyName());
        assertEquals("USD", convertCmd.getTermsCurrencyName());
        assertEquals(100.00d, convertCmd.getBaseAmount(), 0.0001d);
    }

    @Test(expected = ConvertCmd.NotConvertCommandException.class)
    public void parseFailed() throws ConvertCmd.NotConvertCommandException {
        // Given
        String cmdLine = "AUD100.00 in USD";

        // When
        ConvertCmd convertCmd = ConvertCmd.parse(cmdLine);

        // Then
        // expects to throw NotConvertCommandException
    }

}
