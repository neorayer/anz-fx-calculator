package me.ucu.exam.anz.fxcalculator.cli;

import me.ucu.exam.anz.fxcalculator.Calculator;
import me.ucu.exam.anz.fxcalculator.currency.NoSuchCurrencyException;
import me.ucu.exam.anz.fxcalculator.service.UnableToFindRateException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static me.ucu.exam.anz.fxcalculator.cli.Consts.*;

/**
 * CalculatorCli is the user 'command line interface' implementation class.
 * It provides continuous interaction in console.
 */
public class CalculatorCli {

    private final static String PROMPT = "%> ";

    private BufferedReader br;

    private Calculator calculator;

    /**
     * Constructor if CalculatorCli class. A Calculator need to be injected
     * to implement calculation logic.
     *
     * @param calculator the Calculator class in fxcalculator-core library.
     */
    public CalculatorCli(Calculator calculator) {
        this.calculator = calculator;
        this.br = new BufferedReader(new InputStreamReader(System.in));
    }

    /**
     * Read a command line from console standard input.
     *
     * @return the command line string
     * @throws IOException
     */
    private String readCommand() throws IOException {
        return br.readLine().trim();
    }

    /**
     * Print a string onto console standard output.
     *
     * @param s the string need to be output.
     * @throws IOException
     */
    private void print(String s) throws IOException {
        System.out.print(s);
    }

    /**
     * Print a string and additional line return onto console standard output.
     *
     * @param s the string need to be output.
     * @throws IOException
     */
    private void println(String s) throws IOException {
        System.out.println(s);
    }

    /**
     * Print value with format onto console standard output.
     *
     * @param format
     * @param args
     * @throws IOException
     */
    private void printf(String format, Object... args) throws IOException {
        System.out.printf(format, args);
    }

    /**
     * Execute the main program.
     *
     * @throws IOException
     */
    public void run() throws IOException {
        println(RSP_WELCOME);
        println(RSP_HELP);
        boolean isQuit = false;
        while (!isQuit) {
            print(PROMPT);
            String cmd = readCommand();
            switch (cmd.toUpperCase()) {
                case CMD_Q:
                case CMD_QUIT:
                case CMD_EXIT:
                    isQuit = true;
                    break;
                case CMD_HELP:
                    println(RSP_HELP);
                    break;
                default:
                    try {
                        String s = processConvertCommand(cmd);
                        println(s);
                    } catch (ConvertCmd.NotConvertCommandException e) {
                        println(RSP_UNKNOWN_CMD);
                        println(RSP_HELP);
                    } catch (NoSuchCurrencyException e) {
                        printf("%s %s\n", RSP_NO_SUCH_CURRENCY, e.getCurrencyName());
                    } catch (UnableToFindRateException e) {
                        printf("%s for %s\n", RSP_UNABLE_TO_FIND_RATE, e.getCurrencyPairName());
                    }
                    break;
            }
        }
        println("Bye");
    }

    /**
     * Process the currency converting command.
     *
     * @param cmdLine the input command line
     * @return the response after the calculation.
     * @throws ConvertCmd.NotConvertCommandException if it isn't a currency convert command
     * @throws UnableToFindRateException             if the currency pair rate cannot be found
     * @throws NoSuchCurrencyException               if there isn't such currency in system
     */
    public String processConvertCommand(String cmdLine)
            throws ConvertCmd.NotConvertCommandException, UnableToFindRateException, NoSuchCurrencyException {
        ConvertCmd cmd = ConvertCmd.parse(cmdLine);
        return calculator.convertToDescription(
                cmd.getBaseCurrencyName(),
                cmd.getTermsCurrencyName(),
                cmd.getBaseAmount());
    }
}
