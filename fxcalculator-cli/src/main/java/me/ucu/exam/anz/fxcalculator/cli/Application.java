package me.ucu.exam.anz.fxcalculator.cli;

import me.ucu.exam.anz.fxcalculator.Calculator;
import me.ucu.exam.anz.fxcalculator.currency.Currency;
import me.ucu.exam.anz.fxcalculator.currency.CurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.MemEmbeddedCurrencyReader;
import me.ucu.exam.anz.fxcalculator.currency.NoSuchCurrencyException;
import me.ucu.exam.anz.fxcalculator.pair.DirectCurrencyPair;
import me.ucu.exam.anz.fxcalculator.service.CurrencyPairService;
import me.ucu.exam.anz.fxcalculator.service.CurrencyPairServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;

import javax.annotation.PostConstruct;
import java.io.IOException;

/**
 * Application is the main entry class of FX Calculator application.
 * It is implemented on Spring Framework via Spring boot project.
 * <p>
 * This class is used as configuration file as well.
 * It load properties from yaml file resources/application.yml .
 */
@SpringBootApplication
public class Application {

    private static Logger logger = LoggerFactory.getLogger(Application.class);

    @Autowired
    private AppProperties appProperties;

    @Autowired
    private CurrencyReader currencyReader;

    @Autowired
    private CurrencyPairService currencyPairService;

    @Autowired
    private Calculator calculator;

    @Autowired
    private CalculatorCli calculatorCli;

    public static void main(String[] args) {
        new SpringApplicationBuilder(Application.class)
                .logStartupInfo(false)
                .run(args);
    }

    @Bean
    @ConfigurationProperties("fxcalculator")
    public AppProperties appProperties() {
        return new AppProperties();
    }

    @Bean
    public MemEmbeddedCurrencyReader currencyReader() {
        MemEmbeddedCurrencyReader reader = new MemEmbeddedCurrencyReader();
        reader.setCurrencies(appProperties.getCurrencies());
        return reader;
    }

    @Bean
    public CurrencyPairService currencyPairService() {
        CurrencyPairServiceImpl service = new CurrencyPairServiceImpl();
        appProperties.getDirectCurrencyPairs().stream()
                .map(this::getDirectCurrencyPair)
                .filter(pair -> pair != null)
                .forEach(service::put);
        return service;
    }

    @Bean
    public Calculator calculator() {
        return new Calculator(currencyReader, currencyPairService);
    }

    @Bean
    public CalculatorCli calculatorCli() {
        return new CalculatorCli(calculator);
    }

    /**
     * Create a DirectCurrencyPair object from PairItem object.
     *
     * @param pairItem the PairItem object which is read from application.yml file
     * @return the DirectCurrencyPair object
     */
    private DirectCurrencyPair getDirectCurrencyPair(AppProperties.PairItem pairItem) {
        try {
            Currency baseCurrency = currencyReader.read(pairItem.getBaseName());
            Currency termsCurrency = currencyReader.read(pairItem.getTermsName());
            return new DirectCurrencyPair(baseCurrency, termsCurrency, pairItem.getDecimalPlaces(), pairItem.getRate());
        } catch (NoSuchCurrencyException e) {
            logger.warn("", e);
            return null;
        }
    }

    /**
     * This is the real program executing start point.
     *
     * @throws IOException
     */
    @PostConstruct
    public void run() throws IOException {
        calculatorCli.run();
    }
}

