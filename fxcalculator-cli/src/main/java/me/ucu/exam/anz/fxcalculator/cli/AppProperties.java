package me.ucu.exam.anz.fxcalculator.cli;

import me.ucu.exam.anz.fxcalculator.currency.Currency;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.ArrayList;
import java.util.List;

/**
 * The application configuration property class which is corresponding with application.yml.
 */
@ConfigurationProperties("fxcalculator")
public class AppProperties {

    private List<Currency> currencies = new ArrayList<>();

    private List<PairItem> directCurrencyPairs = new ArrayList<>();

    public List<Currency> getCurrencies() {
        return currencies;
    }

    public List<PairItem> getDirectCurrencyPairs() {
        return directCurrencyPairs;
    }

    /**
     * The corresponding class with the item of 'directCurrencyPairs' in application.yml
     * for example:
     * - name: AUD/USD
     * rate: 0.8371
     * decimalPlaces: 4
     */
    public static class PairItem {

        private String name;

        private String baseName;

        private String termsName;

        private int decimalPlaces;

        private double rate;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            name = name.trim();
            String[] ss = name.split("/");
            baseName = ss[0].trim();
            termsName = ss[1].trim();
            this.name = name;
        }

        public int getDecimalPlaces() {
            return decimalPlaces;
        }

        public void setDecimalPlaces(int decimalPlaces) {
            this.decimalPlaces = decimalPlaces;
        }

        public double getRate() {
            return rate;
        }

        public void setRate(double rate) {
            this.rate = rate;
        }

        public String getBaseName() {
            return baseName;
        }

        public String getTermsName() {
            return termsName;
        }
    }
}
