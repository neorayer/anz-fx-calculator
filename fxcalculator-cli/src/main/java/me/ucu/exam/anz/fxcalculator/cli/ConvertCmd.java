package me.ucu.exam.anz.fxcalculator.cli;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The currency converting command class. The most important method is parse(String) which
 * parses a command line into  ConvertCmd object.
 */
public class ConvertCmd {

    private String baseCurrencyName;

    private String termsCurrencyName;

    private double baseAmount;

    /**
     * Get a ConvertCmd object by parsing the command line
     *
     * @param commandLine the command line need to be parsed
     * @return the corresponding ConvertCmd object
     * @throws NotConvertCommandException if it isn't a legal currency convert command.
     */
    public static ConvertCmd parse(String commandLine) throws NotConvertCommandException {
        commandLine = commandLine.trim();
        // regex: ^([a-zA-Z]{3})\s+(\d+\.?\d*)\s+([i|I][n|N])\s+([a-zA-Z]{3})$
        String regex = "^([a-zA-Z]{3})\\s+(\\d+\\.?\\d*)\\s+([i|I][n|N])\\s+([a-zA-Z]{3})$";
        if (!commandLine.matches(regex)) {
            throw new NotConvertCommandException();
        }

        ConvertCmd cmd = new ConvertCmd();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(commandLine);
        while (matcher.find()) {
            cmd.setBaseCurrencyName(matcher.group(1));
            cmd.setBaseAmount(Double.parseDouble(matcher.group(2)));
            cmd.setTermsCurrencyName(matcher.group(4));
        }
        return cmd;
    }

    public String getBaseCurrencyName() {
        return baseCurrencyName;
    }

    public void setBaseCurrencyName(String baseCurrencyName) {
        this.baseCurrencyName = baseCurrencyName;
    }

    public String getTermsCurrencyName() {
        return termsCurrencyName;
    }

    public void setTermsCurrencyName(String termsCurrencyName) {
        this.termsCurrencyName = termsCurrencyName;
    }

    public double getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(double baseAmount) {
        this.baseAmount = baseAmount;
    }

    public static class NotConvertCommandException extends Exception {
    }
}
