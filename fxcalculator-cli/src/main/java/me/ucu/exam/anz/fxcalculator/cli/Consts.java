package me.ucu.exam.anz.fxcalculator.cli;

public class Consts {

    public final static String CMD_Q = "Q";
    public final static String CMD_QUIT = "QUIT";
    public final static String CMD_EXIT = "EXIT";
    public final static String CMD_HELP = "HELP";

    public final static String RSP_WELCOME = "Welcome to ANZ FX Calculator\n================================";
    public final static String RSP_UNKNOWN_CMD = "Unknown command";
    public final static String RSP_NO_SUCH_CURRENCY = "No such currency";
    public final static String RSP_UNABLE_TO_FIND_RATE = "Unable to find rate";
    public final static String RSP_HELP = "" +
            "usage:\n" +
            "\tAUD 10000 in USD\n" +
            "\n" +
            "commands:\n" +
            "\tq    - quit the calculator\n" +
            "\tquit - same as q\n" +
            "\texit - same as q\n" +
            "\thelp - show this information\n";


}
